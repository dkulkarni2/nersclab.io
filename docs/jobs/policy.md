# Queues and Charges

This page details the QOS and queue usage policies.
[Examples](examples/index.md) for each type of slurm job are available.

---

When a job runs on a NERSC supercomputer, charges accrue against one of the 
user's projects.  The unit of accounting for these charges is the 
"Node Hour", based on the performance of the nodes on Perlmutter. 
The total number of charged hours for a job is a function of:

- the number of nodes and the walltime used by the job, 
- the [QOS](#selecting-a-queue) of the job, and 
- the ["charge factor"](#charge-factors) for the system upon which the job was run. 

Job charging policies, including the intended use of each queue, are 
outlined in more detail under 
["Policies"](../policies/resource-usage.md). This page summarizes
the limits and charges applicable to each queue.

## Selecting a Queue

Jobs are submitted to different queues depending on the queue
constraints and the user's desired outcomes. Each queue corresponds 
to a "Quality of Service" (QOS): Each queue has a different service 
level in terms of priority, run and submit limits, walltime limits, 
node-count limits, and cost. At NERSC, the terms "queue" and "QOS"
are often used interchangeably.

Most jobs are submitted to the "regular" queue. A user who needs fast 
turnaround while they are using the large telescope could 
prearrange with NERSC to use the `realtime` queue
for these runs. The user with the scientific emergency will incur a 
higher-than-regular charge, while a user
who can be flexible about their required runtime is rewarded with
a substantial discount.

## Assigning Charges

Users who are members of more than one project can select which one
should be charged for their jobs by default. In [Iris](https://iris.nersc.gov/),
under the "Compute" tab in the user view, select the project you wish to make
default.

To charge to a non-default project, use the `-A projectname` flag in Slurm,
either in the Slurm directives preamble of your script, e.g.,

```
#SBATCH -A myproject
```

or on the command line when you submit your job, e.g., 
`sbatch -A myproject ./myscript.sl`.

!!! warning
	For users who are members of multiple NERSC projects,
	charges are made to the default project, as set
	in [Iris](https://iris.nersc.gov), unless the `#SBATCH
	--account=<NERSC project>` flag has been set.

## Calculating Charges

The cost of a job is computed in the following manner:
$$ \text{walltime in hours} \times \text{number of nodes} \times \text{QOS factor}
\times \text{charge factor} $$.

!!! example
    The charge for a job that runs for 40 minutes on 3 CPU nodes in the 
    preempt QOS (QOS factor of 0.5) would be calculated
    $$ \frac{40\\ \text{mins}}{60\\ \text{min/hr}} \times 3\\ \text{nodes} 
    \times 0.5 \times 1\\ \text{charged-hours/node-hour} = \frac{2}{3} \times 3 
    \times 0.5 \times 1 = 1.0\\ \text{charged hours}.$$

!!! example
    A job which ran for 35 minutes on 3 GPU nodes on Perlmutter with
    the regular QOS would be charged:
    $$ \frac{35}{60}\\ \text{hours} \times 3\\ \text{nodes} \times 1 = 1.75\\ \text{charged hours} $$

!!! note 
    Jobs in the "shared" QOS are only charged for the fraction of the
    node used. 

!!! example
    A job which ran for 10 hours on 2 GPUs in the shared QOS on Perlmutter GPU
    would be charged:
    $$ 10\\ \text{hours} \times (2 \text{GPUs}/4) \times 1 = 5\\ \text{charged hours} $$

!!! note
    Jobs are charged according to the resources they made unavailable
    for other jobs, i.e., the number of nodes reserved (regardless
    of use) and the actual walltime used (regardless of the specified
    limit).

## Charge Factors

Charge factors on Perlmutter are based on the performance of the Perlmutter
nodes and are therefore equal to one. Future systems may have different
charge factors.

| Architecture                               | Charge Factor | 
| ------------------------------------------ | ------------- |
| Perlmutter CPU                             | 1             |
| Perlmutter GPU                             | 1             |

!!! note
    Perlmutter GPU and CPU allocations are separate pools that can be used
    only on the respective resource and cannot be exchanged.

## QOS Cost Factor: Charge Multipliers and Discounts

The QOS cost factor is a function of which queue a job runs in.
Jobs in the `preempt` queue, on the other hand, receive
a substantial discount in exchange for flexibility about walltime, and jobs in the
overrun queue run free of charge.

| QOS                   | QOS Factor  | Conditions                                                   |
|-----------------------|-------------|--------------------------------------------------------------|
| regular               | 1           | (standard charge factor)                                     |
| overrun               | 0           | available only when allocation is exhausted                  |
| preempt               | 0.25 or 0.5 | guaranteed 2-hr minimum walltime, then subject to preemption |

## QOS Limits and Charges

### Perlmutter GPU

| QOS                           | Max nodes    | Max time (hrs) | Submit limit | Run limit | Priority  | QOS Factor | Charge per Node-Hour |
|-------------------------------|--------------|----------------|--------------|-----------|-----------|------------|----------------------|
| regular                       | -            | 12             | 5000         | -         | medium    | 1          | 1                    |
| interactive                   | 4            | 4              | 2            | 2         | high      | 1          | 1                    |
| jupyter                       | 4            | 6              | 1            | 1         | high      | 1          | 1                    |
| debug                         | 8            | 0.5            | 5            | 2         | medium    | 1          | 1                    |
| shared[^shared]               | 0.5          | 12             | 5000         | -         | medium    | 1          | 1[^shared]           |
| preempt                       | 128          | 24 (preemptible after two hours) | 5000 | - | medium  | 0.25       | 0.25                 |
| [overrun](../policies/resource-usage.md#p-overrun) | -   | 12 | 5000         | -         | very low  | 0          | 0                    |
| [realtime](../policies/resource-usage.md#p-realtime) | custom | custom | custom | custom | very high | 1          | 1                    |

 * Nodes allocated by a "regular" QOS job are exclusively used by the
   job.

 * Jobs in the preemptible queue can be preempted after two
   hours. Jobs can be automatically requeued after preemption using
   the `--requeue` sbatch flag. See the [Preemptible Jobs
   section](examples/index.md#preemptible-jobs) for details.

 * NERSC's JupyterHub uses the "jupyter" QOS to start Jupyter notebook
   servers on compute nodes.  Other uses of the QOS are currently not 
   authorized, and the QOS is monitored for unauthorized use.

 * Jobs may run on the "standard" Perlmutter GPU nodes or on the subset of GPU
   nodes which have double the GPU-attached memory. To specifically request
   these higher-bandwidth memory nodes, use  `-C gpu&hbm80g` in your job script
   instead of `-C gpu`. Jobs with this constraint must use 256 or fewer nodes.

!!! warning "Shared GPU Jobs"
    GPU jobs in the `shared` queue may request 1 or 2 GPUs and will be allocated
    a corresponding 16 CPU cores and 64 GB RAM or 32 CPU cores and 128 GB RAM,
    respectively.

### Perlmutter CPU

| QOS                           | Max nodes    | Max time (hrs) | Submit limit | Run limit | Priority  | QOS Factor | Charge per Node-Hour |
|-------------------------------|--------------|----------------|--------------|-----------|-----------|------------|----------------------|
| regular                       | -            | 12             | 5000         | -         | medium    | 1          | 1                    |
| interactive                   | 4            | 4              | 2            | 2         | high      | 1          | 1                    |
| jupyter                       | 4            | 6              | 1            | 1         | high      | 1          | 1                    |
| debug                         | 8            | 0.5            | 5            | 2         | medium    | 1          | 1                    |
| shared[^shared]               | 0.5          | 12             | 5000         | -         | medium    | 1          | 1[^shared]           |
| preempt                       | 128          | 24 (preemptible after two hours) | 5000 | - | medium  | 0.5        | 0.5                  |
| [overrun](../policies/resource-usage.md#p-overrun) | -   | 12 | 5000         | -         | very low  | 0          | 0                    |
| [realtime](../policies/resource-usage.md#p-realtime) | custom | custom | custom | custom | very high | 1          | 1                    |

 * Nodes allocated by a "regular" QOS job are exclusively used by the
   job.
   
 * Even though there is no node limit for the regular queue, not 
   all of the projected 3072 nodes are available today. 
   Please check the state of the nodes in the regular queue with
   `sinfo -s -p regular_milan_ss11`

* Jobs in the preemptible queue can be preempted after two
   hours. Jobs can be automatically requeued after preemption using
   the `--requeue` sbatch flag. See the [Preemptible Jobs
   section](examples/index.md#preemptible-jobs) for details.

 * NERSC's JupyterHub uses the "jupyter" QOS to start Jupyter notebook
   servers on compute nodes.  Other uses of the QOS are currently not 
   authorized, and the QOS is monitored for unauthorized use.

### Perlmutter Login

| QOS                                       | Max nodes     | Max time (hrs) | Submit limit | Run limit | Priority | QOS Factor | Charge per Node-Hour |
|-------------------------------------------|---------------|----------------|--------------|-----------|----------|------------|----------------------|
| xfer                                      | 1 (login)     | 48             | 100          | 15        | low      | -          | 0                    |
| [cron](./workflow-tools.md#scrontab)      | 1/128 (login) | 24             | -            | -         | low      | -          | 0                    |
| [workflow](./workflow/workflow-queue.md)  | 0.25 (login)  | 2160           | -            | -         | low      | -          | 0                    |

[^shared]:
Shared jobs are only charged for the fraction of the node resources used.

[^interactive]:
Batch job submission is **not** enabled and the 64-node limit applies
**per project** not per user.

### Discounts

Several QOS's offer reduced charging rates:

 * The "preempt" QOS is charged 50% of the "regular" QOS on CPU nodes and
   25% of "regular" QOS on GPU nodes, with a minimum walltime of two hours
   guaranteed.
 * The "overrun" QOS is free of charge and is only available to projects
   that are out of allocation time. Please refer to the
   [overrun](../../policies/resource-usage#overrun) section for more details.
